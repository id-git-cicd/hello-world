let hello = require('./example.js');

describe("test hello function", () => { 
    it("should say hello message for Younouss", () => {
        let name = 'Younouss';

        let message = hello(name);

        expect(message).toEqual("Hello Younouss!");
    });

    it("should say hello message for Abdessamed", () => {
        let name = 'Abdessamed';

        let message = hello(name);

        expect(message).toEqual("Hello Abdessamed!");
    })
})